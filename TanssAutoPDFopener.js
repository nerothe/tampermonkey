// ==UserScript==
// @name         Tanss Auto PDF opener
// @namespace    nerothe.com
// @description  try to take over the world!
// @version      1.0
// @description  Opens PDF Files from Links in Tanss without any interaction
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/index.php?section=bug&sub=view&*
// @match        *://suwtanss/index.php?section=bug&sub=view&*
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoPDFopener.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoPDFopener.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    for (var ls = document.links, numLinks = ls.length, i=0; i<numLinks; i++){
        var href = ls[i].href;

        if (href.indexOf("/ajax/srv/mail_ajax.php?t=get") != -1 && href.endsWith(".pdf")) {
            ls[i].onclick = (e)=>{
                var elem = e.target;
                while( elem != null && elem.tagName != "A"){
                    elem = elem.parentElement;
                }
                if(elem == null) return;
                var href = elem.href;
                var blob;
                var oReq = new XMLHttpRequest();
                oReq.open("GET",href, true);
                oReq.responseType = "arraybuffer";
                oReq.onload = function (oEvent) {
                    var arrayBuffer = oReq.response; // Note: not oReq.responseText
                    if (arrayBuffer) {
                        blob = new Blob([arrayBuffer], {type: "application/pdf"});
                        console.log(URL.createObjectURL(blob));

                        var cw = window.open("",elem.innerText,"width=1000,height=800");
                        if(!cw) alert("Bitte erlaube Popups!");
                        var url = URL.createObjectURL(blob);
                        cw.document.body.innerHTML = "<iframe src='"+url+"' style='position:absolute;left:0;width:100%;top:0;height:100%'></iframe>";
                        cw.document.body.style.padding = 0;
                        cw.document.title=elem.innerText;
                        cw.document.body.style.margin = 0;
                        cw.document.body.style.overflow= "hidden";
                    }
                };
                oReq.send(null);
                e.preventDefault();

            };

        }
    }
})();