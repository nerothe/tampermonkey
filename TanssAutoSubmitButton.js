// ==UserScript==
// @name         Tanss AutoSubmit Button
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.0
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/index.php?section=intern
// @match        *://suwtanss/index.php?section=intern
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoSubmitButton.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoSubmitButton.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var t = document.createElement("div");
    t.style.position = "absolute";
    t.style.left = "20px";
    t.style.top = "10px";
    t.style.background = "#fff";
    t.style.padding = "10px";
    t.style.borderRadius = "5px";
    t.innerHTML = "Auto Erledigen";

    t.style.cursor = "pointer";
    t.onclick = function(){
        location.href = location.href+"&autosubmit";
    };
    document.body.appendChild(t);
})();