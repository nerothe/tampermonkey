// ==UserScript==
// @name         Tanss Auto Tagesleistungen
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.0
// @author       Nerothe
// @match        *://suwtanss/index.php?section=eigeneTagesleistungen
// @match        *://tanss.schneider-wulf.de/index.php?section=eigeneTagesleistungen
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoTagesleistungen.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoTagesleistungen.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    setTimeout(()=>{
    if(Array.from(document.querySelectorAll("tr[id^=support_]")).filter(i=>i.getElementsByTagName("td")[0].style.backgroundColor=="rgb(0, 255, 0)").length == 0){
        if(document.getElementById("eigene-tagesleistung-bestaetigen-button")){
            window.confirm = function(){return true;}
            document.getElementById("eigene-tagesleistung-bestaetigen-button").click();
        }
    }},1000);
})();