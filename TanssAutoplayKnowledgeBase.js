// ==UserScript==
// @name         Tanss Autoplay Knowledge Base
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.0
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/index.php?section=kb&sub=show&id=*
// @match        *://suwtanss/index.php?section=kb&sub=show&id=*
// @grant        none
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoplayKnowledgeBase.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssAutoplayKnowledgeBase.js
// ==/UserScript==

(function() {
    'use strict';

    if(document.querySelector("*[onclick^='s2t_dateien(12, ']") != null){
        var id = parseInt(document.querySelector("*[onclick^='s2t_dateien(12, ']") .onclick.toString().match(/(?<=s2t_dateien\(12, )(\d*)/g)[0]);

        s2t_dateien(12, id);
        setTimeout(()=>{
            var els = document.querySelectorAll("*[href*='getFile']>span[id^='s2t']");
            for(var i = 0;i<els.length;i++){
                var v = document.createElement("video");
                console.log(els[i].innerText);
                if(els[i].innerText.endsWith(".mp4")){

                    v.src = els[i].parentElement.getAttribute("href");
                    v.setAttribute("controls",true);

                    document.querySelector(".kb_posting").appendChild(v);
                    v.play();
                }
            }
            s2t_dateien_imgRefresh(12, id);
            s2t_dateien_close(12, id);
        },500);
    }
    // Your code here...
})();