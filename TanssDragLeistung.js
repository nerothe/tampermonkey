// ==UserScript==
// @name         Tanss Drag Leistung
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.14
// @author       Nerothe
// @match        *://suwtanss/*
// @match        *://tanss.schneider-wulf.de/*
// @grant        GM_registerMenuCommand
// @grant        GM_setValue
// @grant        GM_getValue
// @require      https://gitlab.com/nerothe/js/nui/raw/master/nui.js
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssDragLeistung.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssDragLeistung.js
// ==/UserScript==

var css = document.createElement("link");
css.rel = "Stylesheet";
css.href= "https://gitlab.com/nerothe/js/nui/raw/master/nui.css";
document.head.appendChild(css);

(function() {
    'use strict';

    var getTanssFavs=function(cb){
        $j.get("/ajax/srv/fav_ajax.php?t=show",(d)=>{
            var e=document.createElement("html");
            e.innerHTML = d;
            var arr = Array.from(e.querySelectorAll("li[id^='favLi_']")).map(i => { return { value: parseInt(i.id.replace(/favLi_\d*_/,"")), name: i.querySelector(".bold").innerText }});
            console.log(arr);
            cb([{name:"Firma im Kontext",value:-1}].concat(arr));
        })
    }

    var getFirmenTickets = function(id,cb){
        console.log("id",id)
        console.log(id)
        if(id == ""){
            cb([{name:"Kein Ticket",value:-1}]);
        }else{
            if(id == -1){
                id = $("aktuelleFirmaID").value;
            }
        }
        $j.post("/ajax/srv/leistung2_ajax.php?t=firmaChange",{fID: parseInt(id+"")},(e)=>{
            console.log(e.tList)
            var i = document.createElement("html");
            i.innerHTML = e.tList;
            if(i.querySelectorAll("optgroup").length > 1)
                cb([{name:"Kein Ticket",value:-1}].concat(Array.from(i.querySelectorAll("optgroup")[1].querySelectorAll("option")).map(k=>{return {name:k.innerText,value:k.value}})))
        });
    }

    var getDateFromDot=function(d){
        return new Date(d.split(".")[2]+"-"+d.split(".")[1]+"-"+d.split(".")[0]);
    }
    var getTanssDate=function(d){
        return d.getDate().toPaddedString(2)+(d.getMonth()+1).toPaddedString(2)+d.getYear().toString().substr(-2,2);
    }
    var autoReload = true;

    var element = $("tagesAnzeigePortal");
    console.log("Ya?: "+!element)



    if(!element){
        setTimeout(()=>{
            element = document.querySelector("#supportAuflistung");// tr[id^='tep_tr_']
            if(element){
                element.style.position = "relative";
                var d = document.createElement("div");
                d.style.width = "100%";
                d.style.height = "80px";
                d.style.top = "50px";
                d.style.position = "absolute";
                element.appendChild(d);

                AddElementDragEvent(d);
            }
        },3000)

    }else{
        console.log(element);
        element.style.position = "relative";
        AddElementDragEvent(element);
    }



    function AddElementDragEvent(element){
        var dragElement = document.createElement("div");
        dragElement.id = "dragElement";
        dragElement.style.height = "10px";
        dragElement.style.backgroundColor = "#424242";
        dragElement.style.position = "absolute";
        dragElement.style.top = "4px";

        element.appendChild(dragElement);

        var isDragging = false;
        var dragPosStart = null;
        element.addEventListener("mousedown",(e,e2)=>{
            console.log(e,e2);
            if(!isDragging && e.button == 0){
                dragElement = document.getElementById("dragElement");
                if(!dragElement){
                    dragElement = document.createElement("div");
                    dragElement.id = "dragElement";
                    dragElement.style.height = "10px";
                    dragElement.style.backgroundColor = "#424242";
                    dragElement.style.position = "absolute";
                    dragElement.style.top = "4px";

                    element.appendChild(dragElement);
                }
                
                var posInDiv = (e.pageX-element.cumulativeOffset()[0]);
                //posInDiv = Math.floor(posInDiv/(element.clientWidth/(14*6)))*(element.clientWidth/(14*6));
                dragElement.style.left = posInDiv+"px";
                dragElement.style.width = 0+"px";
                dragPosStart = posInDiv;
                isDragging = true;
                dragElement.style.display = "block";
                e.preventDefault();
                e.stop
            }
        });
        element.addEventListener("mousemove",(e,e2)=>{
            if(isDragging && e.button == 0){
                var posInDiv = (e.pageX-element.cumulativeOffset()[0]);
                //posInDiv = Math.floor(posInDiv/(element.clientWidth/(14*6)))*(element.clientWidth/(14*6));
                dragElement.style.width = (posInDiv-dragPosStart)+"px";
            }
        });
        element.addEventListener("mouseup",(e,e2)=>{
            if(isDragging && e.button == 0){
                var posInDiv = (e.pageX-element.cumulativeOffset()[0]);

                var time = (dragPosStart/element.clientWidth*14)+5;
                var hour = Math.floor(time);
                var min = Math.floor(((time-hour)*60)/10)*10;
                var len = ((posInDiv-dragPosStart)/element.clientWidth*14);
                dragElement.style.left = posInDiv+"px";
                dragPosStart = posInDiv;
                var hourlen = Math.floor(len);
                var minlen = Math.floor(((len-hourlen)*60)/10)*10;
                console.log(time,hour+"h",min+"m");
                console.log(len,hourlen+"h",minlen+"m");
                dragElement.style.display = "none";
                isDragging = false;


                if(hourlen > 0 || minlen >= 10){
                    var t = openWindow({
                        width: 600,
                        title:"Leistung erstellen",
                        multi:[
                            ".br",
                            {
                                type:"select",
                                name:"firma",
                                value:0,
                                options: async function (e){ return await new Promise(resolve => {getTanssFavs(resolve)}) },
                                onchange: function(){this.multiFields.ticket.renewOptions();}
                            },
                            ".br",
                            {
                                type:"select",
                                name:"ticket",
                                value:0,
                                options: async function (e){ return await new Promise(resolve => {getFirmenTickets((e.multiFields.firma.element.value)?e.multiFields.firma.element.value:-1,resolve);}); }
                            },
                            ".br",
                            ".br",
                            {name:"Text",placeholder:"Leistungs Text",type:"textarea", styleAppend:{width: "500px", height: "100px"}},
                            ".br",
                            ".br",
                            {
                                type:"select",
                                name:"Ltyp",
                                value:3,
                                options:[{name:"Getätigte Leistung",value:1},{name:"Termin",value:3},{name:"Termin Vormerkung",value:2}]
                            },
                            "Uhrzeit:",
                            {
                                type:"time",
                                name:"von",
                                value: hour.toPaddedString(2)+":"+min.toPaddedString(2),
                                styleAppend: { width: "60px", margin:"8px" }                              
                            },
                            "Dauer:",
                            {
                                type:"time",
                                name:"dauer",
                                value: hourlen.toPaddedString(2)+":"+ minlen.toPaddedString(2),  
                                styleAppend: { width: "60px", margin:"8px" }                                         
                            },
                            ".br"
                        ]
                    });
                    t.then((d)=>{
                        var hour = d.von.split(":")[0];
                        var min = d.von.split(":")[1];
                        var hourl = parseInt(d.dauer.split(":")[0]);
                        var minl = parseInt(d.dauer.split(":")[1]);
                        var date = "";
                        if($("datumFeld")==null){
                            date = getDateFromDot(document.querySelector(".naviLeiste").innerText);
                        }else{
                            date = new Date($("datumFeld").value.split(".")[2],$("datumFeld").value.split(".")[1]-1,$("datumFeld").value.split(".")[0]);
                        }

                        var sObj = {
                            datumFeld: getTanssDate(date),
                            vonStd: hour,
                            vonMin: min,
                            dauer: (hourl*60+minl),
                            selTepID: d.Ltyp,
                            text: d.Text
                        };
                        if(d.firma != -1){
                            sObj.firmenID = d.firma;
                        }
                        if(d.ticket != -1){
                            sObj.bugID = d.ticket;
                        }
                        console.log(sObj);
                        $j.post(
                            "/ajax/srv/leistung2_ajax.php?t=save",
                            sObj,
                            (e)=>{
                                if(autoReload)
                                    location.reload()
                            }
                        );
                    },(e)=>console.log(e));
                }
            }
        });

    }
})();