// ==UserScript==
// @name         Tanss Global Keys
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      2.010
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/*
// @match        *://suwtanss/*
// @grant        GM_registerMenuCommand
// @grant        GM_setValue
// @grant        GM_getValue
// @require      https://gitlab.com/nerothe/js/nui/raw/master/nui.js
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssGlobalKeys.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssGlobalKeys.js
// ==/UserScript==

var css = document.createElement("link");
css.rel = "Stylesheet";
css.href = "https://gl.githack.com/nerothe/js/nui/raw/master/nui.css";
document.head.appendChild(css);

var tmpLid = 0;

(function() {
    'use strict';

    GM_registerMenuCommand("AutoReload", () => {
        GM_setValue("AutoReload", !GM_getValue("AutoReload", true));
    }, "Test");
    var autoReload = GM_getValue("AutoReload", true);

    var getDateFromDot = function(d) {
        return new Date(d.split(".")[2] + "-" + d.split(".")[1] + "-" + d.split(".")[0]);
    }
    var getDateFromTanss = function(d) {
        return new Date("20" + d[4] + d[5] + "-" + d[2] + d[3] + "-" + d[0] + d[1]);
    }

    var getTanssDate = function(d) {
        return d.getDate().toPaddedString(2) + (d.getMonth() + 1).toPaddedString(2) + d.getYear().toString().substr(-2, 2);
    }

    var getY_M_D = function(d) {
        return d.getFullYear() + "-" + (d.getMonth() + 1).toPaddedString(2) + "-" + d.getDate().toPaddedString(2);
    }

    var getLeistungText = function(id, cb) {
        $j.get("/index.php?section=leistungen&sub=edit&leistungID=" + id + "&backto2=intern&init=1", (d) => {
            console.log(d);
            console.log($j(d));
        });
    }

    var getLeistungInfoOld = function(id, cb) {
        $j.get("/ajax/srv/firmenauswahl_suche_v4.php?t=s&q=" + id + "&rt=&targetField=", (d) => {
            var result = d.querySelector("result[id='" + id + "']");
            var data = {
                text: result.querySelector("text").innerHTML,
                firmenname: result.querySelector("firmenname").innerHTML,
                datum: getDateFrom(result.querySelector("datum").innerHTML),
                von: result.querySelector("von").innerHTML,
                ort: result.querySelector("ort").innerHTML
            };
            cb(data);
        });
    }

    var getLeistungInfo = function(id, cb) {
        $j.get("/index.php?section=leistungen&sub=edit&leistungID=" + id, (d) => {
            var html = document.createElement("html");
            html.innerHTML = d;
            var d = html;
            //var result = d.querySelector("result[id='"+445340+"']");
            var dauer = parseInt(d.querySelector("#dauer").value);
            var data = {
                text: d.querySelector("#text").value,
                firmenID: d.querySelector("#firmenID").value,
                typID: d.querySelector("#typID").value,
                pauseStd: d.querySelector("#pauseStd").value,
                pauseMin: d.querySelector("#pauseMin").value,
                pauseDauer: d.querySelector("#pauseDauer").value,
                textIntern: d.querySelector("#textIntern").value,
                zusTechniker: d.querySelector("#zusTechniker").value,
                auftraggeberID: d.querySelector("#auftraggeberID").value,
                zuweisung: d.querySelector("#zuweisung").value,
                linkTypID: d.querySelector("#linkTypID").value,
                linkID: d.querySelector("#linkID").value,
                bugID: d.querySelector("#bugID").value,
                slcID: d.querySelector("#slcID").value,
                multiLinks: d.querySelector("#multiLinks").value,
                datumFeld: d.querySelector("#datumFeld").value,
                datum: getDateFromTanss(d.querySelector("#datumFeld").value),
                von: d.querySelector("#vonStd").value.padLeft(2, '0') + ":" + d.querySelector("#vonMin").value.padLeft(2, '0'),
                dauer: dauer,
                dauerTime: Math.floor(dauer / 60).toString().padLeft(2, '0') + ":" + (dauer % 60).toString().padLeft(2, '0')
            };
            console.log(data);
            cb(data);
        });
    }

    //ticket http://suwtanss/index.php?section=leistungen&sub=edit&init=1&backto2=bug&bugID=62289&linkTypID=2

    contextMenu((event) => {
        var div = event.target.closest("div");
        if (div.className == "tepSkalaUhr") {
            window.tmpLH = parseInt(div.innerText);
            console.log(div);
            return true;
        }
    }, [{
        name: "Neu",
        elems: [{
            name: "Mit dauer setzen",
            fun: () => {
                var t = openWindow({ title: "Dauer Setzen", multi: [{ name: "dauer", placeholder: "Stunden" }] });
                t.then((d) => {
                    $j.post(
                        "/ajax/srv/leistung2_ajax.php?t=save", {
                            datumFeld: getTanssDate(new Date($("datumFeld").value.split(".")[2], $("datumFeld").value.split(".")[1] - 1, $("datumFeld").value.split(".")[0])),
                            vonStd: tmpLH,
                            vonMin: 0,
                            dauer: Math.ceil(d.dauer * 60),
                            selTepID: 3
                        },
                        (e) => {
                            if (autoReload)
                                location.reload()
                        }
                    );
                });
            }
        }]
    }]);

    contextMenu((event) => {
        var a = event.target.closest("a");
        if (a.href == "#") {
            var im = a.querySelector("img");
            if (typeof(im) == "object" && "onclick" in im)
                a.href = im.onclick.toString().replace("window.open('", "").replace("')", "");
        }
        var checkstr = "";
        if (a && a.href) {
            if (a.getAttribute("href") == "#" && a.childNodes.length == 1 && a.childNodes[0].tagName == "IMG") {
                var img = a.childNodes[0];
                checkstr = img.onclick.toString();
            } else {
                checkstr = a.href;
            }
        }
        if (checkstr != "" && (checkstr.indexOf("sub=edit") != -1 || checkstr.indexOf("sub=show") != -1) && checkstr.indexOf("leistungID=") != -1) {
            tmpLid = checkstr.split("leistungID=")[1].split("&")[0];
            return true;
        }
        return false;
    }, [{
            name: "Öffnen",
            fun: () => {
                window.open("/index.php?section=leistungen&sub=edit&leistungID=" + tmpLid, '_blank')
            }
        },
        {
            name: "Zeit",
            elems: [{
                    name: "Zeit und Dauer setzen",
                    fun: () => {
                        getLeistungInfo(tmpLid, (data) => {
                            var t = openWindow({
                                title: "Dauer Setzen",
                                multi: [
                                    "Uhrzeit", { name: "von", type: "time", value: data.von, styleAppend: { width: "60px", margin: "8px" } },
                                    "Dauer", { name: "dauer", type: "time", value: data.dauerTime, styleAppend: { width: "60px", margin: "8px" } }
                                ]
                            });
                            t.then((d) => {
                                var parts = d.von.split(":");
                                var h = parseInt((parts.length >= 1) ? parts[0] : 8);
                                var m = parseInt((parts.length > 1) ? parts[1] : 0);
                                var parts = d.dauer.split(":");
                                var hd = parseInt((parts.length >= 1) ? parts[0] : 8);
                                var md = parseInt((parts.length > 1) ? parts[1] : 0);
                                $j.post(
                                    "/ajax/srv/leistung2_ajax.php?t=save", {
                                        lstID: tmpLid,
                                        savedID: tmpLid,
                                        vonStd: h,
                                        vonMin: m,
                                        dauer: (hd * 60 + md),
                                        datumFeld: getTanssDate(data.datum)
                                    },
                                    (e) => {
                                        if (autoReload)
                                            location.reload()
                                    }
                                );
                            });
                        });
                    }
                },
                {
                    name: "Datum setzen",
                    fun: () => {
                        console.log("GETINFO")
                        getLeistungInfo(tmpLid, (data) => {
                            console.log("GOTINFO", data);
                            var date = data.datum;

                            var t = openWindow({ title: "Datum setzen", multi: [{ name: "date", type: "date", value: getY_M_D(date) }] });
                            t.then((d) => {

                                var parts = data.von.split(":");
                                var h = (parts.length >= 1) ? parts[0] : 8;
                                var m = (parts.length > 1) ? parts[1] : 0;
                                $j.post(
                                    "/ajax/srv/leistung2_ajax.php?t=save", {
                                        lstID: tmpLid,
                                        savedID: tmpLid,
                                        vonStd: parseInt(h),
                                        vonMin: parseInt(m),
                                        datumFeld: getTanssDate(new Date(d.date))
                                    },
                                    (d, e) => {
                                        if (autoReload)
                                            location.reload()
                                    }
                                );


                            });

                        });
                    }
                },
                {
                    name: "Um 1d Verschieben",
                    fun: () => {
                        console.log("GETINFO")
                        getLeistungInfo(tmpLid, (data) => {
                            console.log("GOTINFO", data);
                            var date = data.datum;
                            date.setDate(date.getDate() + 1);

                            $j.post(
                                "/ajax/srv/leistung2_ajax.php?t=save", {
                                    lstID: tmpLid,
                                    savedID: tmpLid,
                                    datumFeld: getTanssDate(date),
                                    vonStd: data.von.split(":")[0],
                                    vonMin: data.von.split(":")[1],
                                },
                                (d, e) => {
                                    if (autoReload)
                                        location.reload()
                                }
                            );
                        });
                    }
                }
            ]
        },
        {
            name: "Duplizieren",
            fun: () => {
                getLeistungInfo(tmpLid, (data) => {
                    console.log("GOTINFO", data);
                    var date = data.datum;

                    var t = openWindow({ title: "Datum setzen", multi: [{ name: "date", type: "date", value: getY_M_D(date) }] });
                    t.then((d) => {
                        var parts = data.von.split(":");
                        var h = (parts.length >= 1) ? parts[0] : 8;
                        var m = (parts.length > 1) ? parts[1] : 0;
                        $j.post(
                            "/ajax/srv/leistung2_ajax.php?t=save", {
                                text: data.text,
                                firmenID: data.firmenID,
                                typID: data.typID,
                                pauseStd: data.pauseStd,
                                pauseMin: data.pauseMin,
                                pauseDauer: data.pauseDauer,
                                textIntern: data.textIntern,
                                zusTechniker: data.zusTechniker,
                                auftraggeberID: data.auftraggeberID,
                                zuweisung: data.zuweisung,
                                linkTypID: data.linkTypID,
                                linkID: data.linkID,
                                bugID: data.bugID,
                                slcID: data.slcID,
                                multiLinks: data.multiLinks,
                                vonStd: parseInt(h),
                                vonMin: parseInt(m),
                                datumFeld: getTanssDate(new Date(d.date))
                            },
                            (d, e) => {
                                if (autoReload)
                                    location.reload()
                            }
                        );


                    });

                });
            }
        },
        {
            name: "Löschen",
            fun: () => {
                $j.post("/ajax/srv/support_ajax.php?t=del&id=" + tmpLid, {},
                    (d) => {
                        if (autoReload)
                            location.reload()
                    });
            }
        },
        {
            name: "Quick Edit",
            fun: () => {
                getLeistungInfo(tmpLid, (data) => {
                    var t = openWindow({
                        title: "Quick Edit",
                        width: 600,
                        multi: [
                            { name: "text", type: "textarea", placeholder: "Text", value: data.text, styleAppend: { width: "500px", height: "100px" } }, ".br",
                            { name: "datum", type: "date", value: getY_M_D(data.datum), styleAppend: { width: "160px", margin: "8px" } },
                            "Uhrzeit:", { name: "von", type: "time", value: data.von, styleAppend: { width: "60px", margin: "8px" } },
                            "Dauer:", { name: "dauer", type: "time", value: data.dauerTime, styleAppend: { width: "60px", margin: "8px" } }
                        ]
                    });
                    t.then((d) => {
                        var parts = d.von.split(":");
                        var h = parseInt((parts.length >= 1) ? parts[0] : 8);
                        var m = parseInt((parts.length > 1) ? parts[1] : 0);
                        var parts = d.dauer.split(":");
                        var hd = parseInt((parts.length >= 1) ? parts[0] : 8);
                        var md = parseInt((parts.length > 1) ? parts[1] : 0);
                        $j.post(
                            "/ajax/srv/leistung2_ajax.php?t=save", {
                                lstID: tmpLid,
                                savedID: tmpLid,
                                text: d.text,
                                dauer: (hd * 60 + md),
                                vonStd: h,
                                vonMin: m,
                                datumFeld: getTanssDate(new Date(d.datum))
                            },
                            (d, e) => {
                                if (autoReload)
                                    location.reload()
                            }
                        );


                    });

                });
            }
        },
        {
            name: "LeistungsTyp",
            elems: [{
                    name: "Getätigte Leistung",
                    fun: () => {
                        $j.post("/ajax/srv/leistung2_ajax.php?t=save", { lstID: tmpLid, savedID: tmpLid, selTepID: 1 },
                            (d) => {
                                if (autoReload)
                                    location.reload()
                            });
                    }
                },
                {
                    name: "Termin",
                    fun: () => {
                        $j.post("/ajax/srv/leistung2_ajax.php?t=save", { lstID: tmpLid, savedID: tmpLid, selTepID: 3 },
                            (d) => {
                                if (autoReload)
                                    location.reload()
                            });
                    }
                },
                {
                    name: "Termin Vormerkung",
                    fun: () => {
                        $j.post("/ajax/srv/leistung2_ajax.php?t=save", { lstID: tmpLid, savedID: tmpLid, selTepID: 2 },
                            (d) => {
                                if (autoReload)
                                    location.reload()
                            });
                    }
                },
                {
                    name: "Bereitschaft",
                    fun: () => {
                        $j.post("/ajax/srv/leistung2_ajax.php?t=save", { lstID: tmpLid, savedID: tmpLid, selTepID: 7 },
                            (d) => {
                                if (autoReload)
                                    location.reload()
                            });
                    }
                }
            ]
        }
    ])

    /*
        $j(window).bind("contextmenu", function(event) {
            console.log(event)
            var a = event.target.closest("a");
            if(a && a.href && a.href.indexOf("sub=edit") != -1 && a.href.indexOf("leistungID=") != -1){
                var id = a.href.split("leistungID=")[1].split("&")[0];
                event.preventDefault();
                if(confirm("Ist "+id+" eine getätigte leistung?")){
                    $j.post("/ajax/srv/leistung2_ajax.php?t=save",{lstID: id,savedID: id,selTepID: 1},(d)=>console.log(d));
                    location.reload();
                }
            }
        });*/
    //index.php?section=leistungen&sub=edit&leistungID=428978&backto2=intern&init=1

    $j(window).bind('keydown', function(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 'l':
                    event.preventDefault();
                    var w = window.open('index.php?section=leistungen&sub=edit&init=1&backto2=null&neuesFenster=1', '_blank', 'width=1230,height=735,resizable=yes,scrollbars=yes,top=50,left=50');
                    var date = $j("#datumFeld").val();

                    w.addEventListener('load', function() {
                        w.document.getElementById("datumFeld").value = date.replace(/\.20(\d\d)/g, "$1").replace(/\./g, "");
                        w.le2_datum_change(10);
                        console.log("w.document.getElementById(datumFeld).value:" + w.document.getElementById("datumFeld").value);
                    }, false);

                    le2_datum_change(10);
                    break;
                case 'o':
                    event.preventDefault();
                    document.querySelector("a[href^='index.php?section=leistungen&sub=edit&init=1&backto2=bug']").click();
                    break;
            }
        }
    });
    // Your code here...
})();
