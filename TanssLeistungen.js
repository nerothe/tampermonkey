// ==UserScript==
// @name         Tanss Leistungen
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.0
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/index.php?section=leistungen&sub=edit*
// @match        *://suwtanss/index.php?section=leistungen&sub=edit*
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssLeistungen.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssLeistungen.js
// @grant        none
// ==/UserScript==


(function() {
    'use strict';
    alert = function(){return true;};
    confirm = function(){return true;};
    $j(window).bind('keydown', function(event) {
        if (event.ctrlKey || event.metaKey) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 's':
                    event.preventDefault();
                    $j("#le2submit2").click();
                    setTimeout(function(){
                        $j($j(".ui-dialog-buttonset")[0]).find("button")[1].click();
                    },300);
                    break;
                case 'd':
                    event.preventDefault();
                    $j("#le2submit2").click();
                    setTimeout(function(){
                        $j($j(".ui-dialog-buttonset")[0]).find("button")[0].click();
                    },300);
                    break;
                case 'g':
                    event.preventDefault();
                    zuwsel.show('leistung_edit', 'firmenID', 'linkTypID', 'linkID', true, false, false);
                    setTimeout(()=>{zuwsel.selectKundeGenerell(parseInt($("firmenID").value));},500);
                    break;
            }
        }
    });


    $(typID).value = 140;
    var pContainer = document.getElementById("v4_pageContainer");

    pContainer.style.border="none";
    pContainer.style.marginTop="20px";
    pContainer.style.borderRadius= "7px";
    pContainer.style.overflow= "hidden";
    pContainer.style.boxShadow= "0 0 7px rgba(0,0,0,0.5)";

    var t = document.createElement("div");
    t.style.position = "absolute";
    t.style.left = "20px";
    t.style.top = "10px";
    t.style.background = "#fff";
    t.style.padding = "10px";
    t.style.borderRadius = "5px";
    t.innerHTML = "Nächster Tag";

    t.style.cursor = "pointer";
    t.onclick = function(){
        var date = document.getElementById("datumFeld");
        var parts = date.value.match(/.{1,2}/g);
        var nDate = Date.parse(parts[1]+" "+parts[0]+" 20"+parts[2]);
        var dat = new Date(nDate.valueOf());
        dat.setDate(dat.getDate() + 1);
        var dd = dat.getDate();
        var mm = dat.getMonth()+1; //January is 0!

        var yyyy = dat.getFullYear().toString();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        date.value = dd+mm+yyyy[2]+yyyy[3];
        le2_datum_change(250);
    };
    document.body.appendChild(t);

    t = document.createElement("div");
    t.style.position = "absolute";
    t.style.left = "20px";
    t.style.top = "60px";
    t.style.background = "#fff";
    t.style.padding = "10px";
    t.style.borderRadius = "5px";
    t.innerHTML = "Vorheriger Tag";

    t.style.cursor = "pointer";
    t.onclick = function(){
        var date = document.getElementById("datumFeld");
        var parts = date.value.match(/.{1,2}/g);

        var nDate = Date.parse(parts[1]+" "+parts[0]+" 20"+parts[2]);
        var dat = new Date(nDate.valueOf());
        dat.setDate(dat.getDate() - 1);

        var dd = dat.getDate();
        var mm = dat.getMonth()+1; //January is 0!

        var yyyy = dat.getFullYear().toString();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }

        date.value = dd+mm+yyyy[2]+yyyy[3];
        le2_datum_change(10);
    };
    document.body.appendChild(t);

    var zuweisung = document.querySelectorAll(".bold.ajaxSelect");
    if(zuweisung.length == 1){
        if(zuweisung[0].innerText == "keine Zuweisung"){
            zuwsel.show('leistung_edit', 'firmenID', 'linkTypID', 'linkID', true, false, false);
            setTimeout(()=>{zuwsel.selectKundeGenerell(parseInt($("firmenID").value));},800);
        }
    }
})();