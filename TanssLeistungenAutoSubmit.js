// ==UserScript==
// @name         Tanss Leistungen AutoSubmit
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.0
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/index.php?section=leistungen&sub=edit*&autosubmit
// @match        *://suwtanss/index.php?section=leistungen&sub=edit*&autosubmit
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssLeistungenAutoSubmit.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssLeistungenAutoSubmit.js
// @grant        none
// ==/UserScript==



   var getTime = function(){
        var date = document.getElementById("datumFeld");
        var parts = date.value.match(/.{1,2}/g);
        var nDate = Date.parse(parts[1]+" "+parts[0]+" 20"+parts[2]);
        var dat = new Date(nDate.valueOf());
        dat.setHours(parseInt(document.querySelector("#bisStd").value));
        dat.setMinutes(parseInt(document.querySelector("#bisMin").value));
        return dat;
    };

(function() {
    'use strict';


    alert = function(){return true;};
    confirm = function(){return true;};

    if(document.getElementById("internRHD").checked && getTime().valueOf() < Date.now()){
        console.log("Auto Save als Leistung");
        $j("#le2submit2").click();
        setTimeout(function(){
            if($j(".ui-dialog-buttonset").length > 0)
                $j($j(".ui-dialog-buttonset")[0]).find("button")[0].click();
            setTimeout(()=>{
                if(Array.from(document.querySelectorAll("span")).filter(txt=>txt.innerText==("Kunde (generell)")).length > 0 ){
                    Array.from(document.querySelectorAll("span")).filter(txt=>txt.innerText==("Kunde (generell)"))[0].parentElement.click();
                    setTimeout(()=>{
                        $j("#le2submit2").click();
                    },400);
                }
                setTimeout(()=>{
                    window.close();
                    //window.location = "index.php?section=intern&lastId="+$F('savedID')+"&autosubmit";
                },700);
            },300);
        },300);
    }else{

        if(getTime().valueOf() > Date.now() && window.opener){
            window.opener.autoSubmitPast = true;
        }
        window.close();
        //window.location = "index.php?section=intern&lastId="+$F('savedID')+"&autosubmit";
    }

})();