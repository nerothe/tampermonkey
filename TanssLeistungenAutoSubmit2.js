// ==UserScript==
// @name         Tanss Leistungen AutoSubmit2
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.0
// @author       Nerothe
// @match        *://tanss.schneider-wulf.de/index.php?section=inter*&autosubmit
// @match        *://suwtanss/index.php?section=inter*&autosubmit
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssLeistungenAutoSubmit2.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssLeistungenAutoSubmit2.js
// @grant        none
// ==/UserScript==

function getParams ()
{
    var result = {};
    var tmp = [];

    location.search
        .substr (1)
        .split ("&")
        .forEach (function (item)
        {
            tmp = item.split ("=");
            result [tmp[0]] = decodeURIComponent (tmp[1]);
        });
    return result;
}
function finishAutoSubmit(){
    console.log("CHANGE LOCATION");
    setTimeout(function(){
        window.location.href = window.location.href.replace("&autosubmit","");
    },100);
}

(function() {
    'use strict';
    alert = function(){return true;};
    confirm = function(){return true;};
    window.lastLink = "";
    console.log("autorun");
    var firstElem = document.querySelector("a[href^='index.php?section=leistungen&sub=edit&init=1&leistungID=']");
    console.log(firstElem);
    if(firstElem){
        window.currentRow = firstElem.parentElement.parentElement;
        doRow(window.currentRow);
    }
    window.popupCallback=()=>{
        if(!window.autoSubmitPast){
            console.log("DO NEXT ROW");
            doRow(window.currentRow.nextElementSibling);
        }else{
            console.log("PAST");
            return finishAutoSubmit();
        }
    };
    function doRow(currentRow){
        window.currentRow = currentRow;
        console.log("DOROW!");
        console.log(currentRow);
        if(!currentRow){
            console.log("NO ROWS LEFT");
            return finishAutoSubmit();
        }
        var nextLink = currentRow.querySelector("a[href^='index.php?section=leistungen&sub=edit']");
        console.log("Open Window:"+nextLink.href+"&autosubmit");
        setTimeout(()=>{
            if(lastLink == nextLink.href+"&autosubmit") {
                console.log("SAME AS LAST");
                return finishAutoSubmit();
            }
            lastLink = nextLink.href+"&autosubmit";
            var cw = window.open(lastLink,"AutoSubmitter", "height=500,width=500");
            cw.onbeforeunload = function(){
                cw.opener.popupCallback();
                cw.opener.console.log("hey");
            };
        },100);
    }
})();