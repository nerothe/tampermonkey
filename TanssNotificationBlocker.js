// ==UserScript==
// @name         TanssNotificationBlocker
// @namespace    https://gitlab.com/nerothe/
// @description  try to take over the world!
// @version      1.14
// @author       Nerothe
// @match        *://suwtanss/*
// @match        *://tanss.schneider-wulf.de/*
// @downloadURL  https://gitlab.com/nerothe/tampermonkey/raw/master/TanssNotificationBlocker.js
// @updateURL    https://gitlab.com/nerothe/tampermonkey/raw/master/TanssNotificationBlocker.js
// ==/UserScript==



(function() {
    'use strict';
    var onAppend = function(elem, f) {
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(m) {
                if (m.addedNodes.length) {
                    f(m.addedNodes)
                }
            })
        })
        observer.observe(elem, { childList: true })
    }
    onAppend(document.body, function(added) {
        for (var k in added) {
            if (added[k].id == "notificationDiv")
                document.querySelectorAll("a[onclick^='confirmNotification(']").forEach(i => i.click());
        }
    })
})();